[GtkTemplate (ui = "/org/gnome/Example/page.ui")]
public class BrowserDemo.PageContents : Adw.Bin {
    public signal void activated (File dest);

    [GtkChild]
    private unowned Gtk.ScrolledWindow scrolled_window;
    [GtkChild]
    private unowned Gtk.SingleSelection selection;
    [GtkChild]
    private unowned Gtk.DirectoryList model;

    public File file { get; construct; }

    private uint initial_selected_pos;
    private double initial_scroll_pos;

    public uint selected_pos {
        get { return selection.selected; }
    }

    public double scroll_pos {
        get { return scrolled_window.vadjustment.value; }
    }

    public PageContents (File file, uint selected_pos, double scroll_pos) {
        Object (file: file);

        initial_selected_pos = selected_pos;
        initial_scroll_pos = scroll_pos;
    }

    [GtkCallback]
    private void notify_loading_cb () {
        if (model.loading)
            return;

        selection.selected = initial_selected_pos;
        scrolled_window.vadjustment.value = initial_scroll_pos;
    }

    [GtkCallback]
    private void row_activated (uint position) {
        var info = selection.get_item (position) as FileInfo;

        if (info.get_file_type () == DIRECTORY) {
            var file = info.get_attribute_object ("standard::file") as File;

            activated (file);
        }
    }

    [GtkCallback]
    private Icon? get_file_icon (FileInfo? info) {
        return info?.get_icon ();
    }

    [GtkCallback]
    private string get_file_name (FileInfo? info) {
        return info?.get_display_name ();
    }

    [GtkCallback]
    private static string get_file_name_sort (FileInfo? info) {
        return info?.get_display_name ();
    }

    [GtkCallback]
    private static bool get_file_is_folder (FileInfo? info) {
        return info?.get_file_type () == DIRECTORY;
    }

    [GtkCallback]
    private static bool get_file_is_hidden (FileInfo? info) {
        return info?.get_is_hidden ();
    }
}

public class BrowserDemo.Page : Adw.NavigationPage {
    public signal void activated (File dest);

    private File file;
    private uint selected_pos;
    private double scroll_pos;

    public Page (File file) {
        this.file = file;

        title = file.get_basename ();
    }

    protected override void showing () {
        var contents = new PageContents (file, selected_pos, scroll_pos);

        contents.activated.connect (dest => {
            activated (dest);
        });

        child = contents;
    }

    protected override void hidden () {
        var contents = child as PageContents;

        selected_pos = contents.selected_pos;
        scroll_pos = contents.scroll_pos;

        child = null;
    }
}

[GtkTemplate (ui = "/org/gnome/Example/window.ui")]
public class BrowserDemo.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Adw.NavigationView nav_view;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        var file = File.new_for_path ("/");
        nav_view.push (create_page (file));
    }

    private Page create_page (File file) {
        var page = new Page (file);

        page.activated.connect (dest => {
            nav_view.push (create_page (dest));
        });

        return page;
    }
}
